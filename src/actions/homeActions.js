export const TOGGLE_TAB = 'TOGGLE_TAB';

export const toggleTab = () => {
  return { type: TOGGLE_TAB }
}

export default {
  TOGGLE_TAB,
  toggleTab
}