import homeActions from './homeActions';
import strategyActions from './strategyActions';

export default {
  homeActions,
  strategyActions
}