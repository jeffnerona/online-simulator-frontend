export const GET_STRATEGIES = 'GET_STRATEGIES';
export const GET_STRATEGIES_SUCCESS = 'GET_STRATEGIES_SUCCESS';
export const GET_STRATEGIES_FAILURE = 'GET_STRATEGIES_FAILURE';
export const ADD_STRATEGY = 'ADD_STRATEGY';

export const addStrategy = payload => {
  return { type: ADD_STRATEGY, payload }
}

export const getStrategies = () => ({
  type: GET_STRATEGIES,
});

export const getStrategiesSuccess = payload => ({
  type: GET_STRATEGIES_SUCCESS,
  payload,
});

export const getStrategiesFailure = error => ({
  type: GET_STRATEGIES_FAILURE,
  error,
});

export default {
  GET_STRATEGIES,
  GET_STRATEGIES_SUCCESS,
  GET_STRATEGIES_FAILURE,
  ADD_STRATEGY,
  getStrategiesSuccess,
  getStrategiesFailure,
  addStrategy
}