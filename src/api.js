import axios from 'axios';
import config from 'config';

const makeRequest = (urlExtension, data = {}) => {
  return axios.get(config.baseUrl + urlExtension, data, {withCredentials: true});
}

export default {
  getStuff: () => makeRequest('meow')
}
