import { connect } from 'react-redux';
import { addStrategy } from 'actions/strategyActions';
import View from './menu';

const mapStateToProps = (state, ownProps) => {
  const { strategies } = state;
  return {
  	count: strategies.list.length
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onClick: (strategy) => {
      dispatch(addStrategy(strategy))
    }
  }
}

const Menu = connect(
  mapStateToProps,
  mapDispatchToProps
)(View)

export default Menu;

