import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/AddRounded';

const customStyles = {
  button: {
    backgroundColor: "#fe9a2c",
    color: "#ffffff",
    width: "48px",
    height: "48px",
  },
  icon: {
    fontSize: 42,
  }
};

export default class Menu extends React.Component {
  addStrategy = () => {
    const index = this.props.count + 1;
    const now = moment();
    const newStrategy = {
      CreateDate: now,
      Description: "Generated Strategy by Click",
      ModifyDate: now,
      Name: `Generated Strategy by Click #${index}`,
      Status: 1
    }
    this.props.onClick(newStrategy);
  }

  render() {
    return (
      <div className="strategies-header">
        <div className="strategies-header-text">
          <h1>Property Wealth Planning Simulator</h1>
        </div>
        <div style={{float:'left'}}>
          <Button
            variant="fab"
            color="primary"
            aria-label="Add"
            style={customStyles.button}
            onClick={() => this.addStrategy()}>
            <AddIcon style={customStyles.icon}/>
          </Button>
        </div>
      </div>
    );
  }
}

Menu.propTypes = {
 addStrategy: PropTypes.func.isRequired
}
Menu.defaultProps = {
 addStrategy: () => console.log('add strategy')
}

// export default Menu;
