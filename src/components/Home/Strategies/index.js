import { connect } from 'react-redux';
// import strategyActions from 'actions/strategyActions';
import { getStrategies } from 'actions/strategyActions';
import View from './strategies';

const mapStateToProps = (state, ownProps) => {
  const { strategies } = state;
  return {
    strategies: strategies.list
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    getStrategies: () => {
      dispatch(getStrategies())
    }
  }
}

const Strategies = connect(
  mapStateToProps,
  mapDispatchToProps
)(View)

export default Strategies;

