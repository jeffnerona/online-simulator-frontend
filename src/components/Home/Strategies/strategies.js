import React from 'react';
import StrategyItem from './strategyItem';
// import PropTypes from 'prop-types';
// import Button from '@material-ui/core/Button';

export default class Strategies extends React.Component {
  componentDidMount() {
    this.props.getStrategies();
  }

  sortStrategies = (strategies) => {
    return strategies.sort((a, b) => new Date(b.CreateDate) - new Date(a.CreateDate));
  }

  renderStrategies = () => {
    const { strategies } = this.props;
    const sortedStrategies = this.sortStrategies(strategies);
    return sortedStrategies.map((strategy) => {
      return <StrategyItem key={strategy.Name} strategy={strategy} />
    })
  }

  render() {
    return (
      <div className="strategies-list">
        {this.renderStrategies()}
      </div>
    );
  }
}

// Strategies.propTypes = {
//  goHome: PropTypes.func.isRequired
// }
// Strategies.defaultProps = {
//  goHome: () => console.log('going home')
// }

// export default Menu;
