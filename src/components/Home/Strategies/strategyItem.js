import React from 'react';
import moment from 'moment';
import { IconButton } from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';

export default class StrategyItem extends React.Component {
  
  formatStrategy = (strategy) => {
    const status = {
      0 : 'Presented',
      1 : 'In Progress',
      2 : 'Completed',
      3 : 'Archived'
    }

    return {
      Name: strategy.Name || '',
      Description: strategy.Description || '',
      CreateDate: strategy.CreateDate ? moment(strategy.CreateDate).format("YYYY-MM-DD") : '',
      ModifyDate: strategy.ModifyDate ? moment(strategy.ModifyDate).format("YYYY-MM-DD") : '',
      Status: strategy.Status ? status[strategy.Status] : ''
    }
  }

  render() {
    const strategy = this.formatStrategy(this.props.strategy);
    return (
      <div className="strategies-item strategies-item-container">
        <div className="strategies-item strategies-content">
          <h3>{strategy.Name}</h3>
          <p>{strategy.Description}</p>
        </div>
        <div className="strategies-item strategies-date-container">
          <div className="strategies-item strategies-created-date">
            <h5>Created On</h5>
            <p>{strategy.CreateDate}</p>
          </div>
          <div className="strategies-item strategies-modified-date">
            <h5>Modified On</h5>
            <p>{strategy.ModifyDate}</p>
          </div>
        </div>
        <div className="strategies-item strategies-menu-container">
          <div className="strategies-item strategies-status">
            <div className="status-container">
              <span>{strategy.Status}</span>
            </div>
          </div>
          <div className="strategies-item strategies-menu-items">
            <IconButton
              aria-label="More"
              onClick={() => { console.log('onclick') }}>
              <MoreVertIcon />
            </IconButton>
          </div>
        </div>
      </div>
    );
  }
}