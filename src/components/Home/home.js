import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Strategies from './Strategies';
import Menu from 'common/Menu';
import './home.css';

export default class Home extends React.Component {
  render() {
    const { activeTab } = this.props;
    return(
      <div>
        <Menu />
        <AppBar position="static" style={{backgroundColor: "transparent", boxShadow: "none"}}>
          <Tabs id="tabs" value={activeTab} onChange={this.props.toggleTab}>
            <Tab label="Active" />
            <Tab label="Archived" />
          </Tabs>
        </AppBar>
        <Strategies />
      </div>
    );
  }
}

Home.propTypes = {
 toggleTab: PropTypes.func.isRequired
}

Home.defaultProps = {
 toggleTab: () => console.log('default click action')
}

// export default Home;
