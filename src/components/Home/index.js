import { connect } from 'react-redux';
import { toggleTab } from 'actions/homeActions';
import View from './home';

const mapStateToProps = (state, ownProps) => {
  const { home } = state;
  const { activeTab } = home;
  return {
    activeTab
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    toggleTab: () => dispatch(toggleTab())
  }
}

const Home = connect(
  mapStateToProps,
  mapDispatchToProps
)(View)

export default Home;

