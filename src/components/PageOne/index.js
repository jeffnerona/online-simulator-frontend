import { connect } from 'react-redux';
// import actions from 'actions';
import View from './pageOne';

const mapStateToProps = (state, ownProps) => {
  return {
    buttonText: 'Hi'
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onClick: () => {
      console.log('Action here');
      // dispatch({type: actions.BASIC_ACTION, text: 'new text'})
    }
  }
}

const PageOne = connect(
  mapStateToProps,
  mapDispatchToProps
)(View)

export default PageOne;

