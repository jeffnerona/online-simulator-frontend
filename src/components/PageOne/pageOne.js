import React from 'react';
import PropTypes from 'prop-types';

const PageOne = props =>
  <div>
      <h1>WHATS UP</h1>
  </div>

PageOne.propTypes = {
  buttonText: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
}
PageOne.defaultProps = {
  buttonText: 'defaultText',
  onClick: () => console.log('default click action')
}

export default PageOne;
