import "babel-polyfill";
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from 'react-redux';
import { store } from 'store';
import CssBaseline from '@material-ui/core/CssBaseline';
import './index.css';
import AppRouter from 'routes';

ReactDOM.render(
  <Provider store={store}>
    <CssBaseline>
      <AppRouter />
    </CssBaseline>
  </Provider>,
  document.getElementById('root')
);