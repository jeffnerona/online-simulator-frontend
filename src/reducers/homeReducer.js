import homeActions from 'actions/homeActions';

const INITIAL_STATE = {
  activeTab: 0
}

const homeReducer = (state = INITIAL_STATE, action) => {
 switch(action.type) {
  case homeActions.TOGGLE_TAB:
    return {
      ...state,
      activeTab: state.activeTab === 0 ? 1 : 0
    }
  default:
    return state
  }
}
export default homeReducer;
