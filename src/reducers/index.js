import { combineReducers } from "redux";
import basicReducer from './basicReducer';
import stuffReducer from './stuffReducer';
import homeReducer from './homeReducer';
import strategiesReducer from './strategiesReducer';

export const reducers = combineReducers({
  text: basicReducer,
  stuff: stuffReducer,
  home: homeReducer,
  strategies: strategiesReducer,
});
