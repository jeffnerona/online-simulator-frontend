import strategyActions from 'actions/strategyActions';

const INITIAL_STATE = {
  list: []
}

const strategiesReducer = (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case strategyActions.GET_STRATEGIES_SUCCESS:
      return {
      	...state,
      	list: action.payload
      }
    case strategyActions.ADD_STRATEGY:
      return {
      	...state,
      	list: state.list.concat(action.payload)
      }
    default:
      return state
  }
}
export default strategiesReducer;
