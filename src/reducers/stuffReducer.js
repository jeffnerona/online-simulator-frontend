import actions from 'actions';
const stuffReducer = (state = {}, action) => {
  switch(action.type) {
    case actions.GOT_STUFF:
      return {
      	...state,
      	file: action.payload.file
      }
    default:
      return state
  }
}
export default stuffReducer;
