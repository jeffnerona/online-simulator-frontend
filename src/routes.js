import React from 'react';
import { Route, BrowserRouter } from 'react-router-dom';
import HomeScreen from 'components/Home';
import PageOneScreen from 'components/PageOne';

const AppRouter = () => (
  <BrowserRouter>
    <div>
      <Route path="/" component={HomeScreen} />
      <Route path="/page1" exact component={PageOneScreen} />
    </div>
  </BrowserRouter>
)

export default AppRouter;

