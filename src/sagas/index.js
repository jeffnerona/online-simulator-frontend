import { takeLatest, put } from 'redux-saga/effects';
import {
  GET_STRATEGIES,
  getStrategiesSuccess,
  getStrategiesFailure
} from 'actions/strategyActions';
// import api from 'api';

const dummyData = {
  strategies: [
    {
      CreateDate: "2018-10-03 15:53:51",
      Description: "Generated Strategy by Click",
      ModifyDate: "2018-10-03 15:53:51",
      Name: "Generated Strategy by Click #1",
      Status: 1
    },
    {
      CreateDate: "2018-10-04 15:53:51",
      Description: "Generated Strategy by Click",
      ModifyDate: "2018-10-04 15:53:51",
      Name: "Generated Strategy by Click #2",
      Status: 1
    }
  ]
}

function* getStrategiesSaga() {
  try {
      // const response = yield call(api.getStrategies);
      const response = {
        data: dummyData.strategies
      }
      yield put(getStrategiesSuccess(response.data));
    } catch (error) {
      console.log('saga fail: ', error);
      yield put(getStrategiesFailure(error));
    }
}

export function* sagas() {
  yield takeLatest(GET_STRATEGIES, getStrategiesSaga);
}